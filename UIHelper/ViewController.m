//
//  ViewController.m
//  UIHelper
//
//  Created by Mac on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import "ViewController.h"
#import "HomeToolItemView.h"
#import "HomeToolItemModel.h"
#import "PresentCustomAnimator.h"
#import "XIBTransferViewController.h"
#import "UIHelperConfigEditView.h"
#import "UIHelperConfigWindowController.h"
@interface ViewController()<NSCollectionViewDataSource,NSCollectionViewDelegate>

@property(nonatomic, strong) NSCollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray<HomeToolItemModel *> *tools;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    [self configData];
    [self configUI];
}

- (void)configData{
    self.tools = [NSMutableArray<HomeToolItemModel *> new];
//    HomeToolItemModel *xibModel = [HomeToolItemModel itemWithTitle:@"XIB转换" iconName:@"" controller:[UIHelperConfigWindowController shareInstance]];
//
//    HomeToolItemModel *diffModel = [HomeToolItemModel itemWithTitle:@"DIFF" iconName:@"" controller:[UIHelperConfigWindowController shareInstance]];
    
    HomeToolItemModel *UIHelperConfigmodel = [HomeToolItemModel itemWithTitle:@"UIHelper 配置" iconName:@"" controller:[UIHelperConfigWindowController shareInstance]];
    
    [self.tools addObjectsFromArray:@[UIHelperConfigmodel]];

}

- (void)configUI{
    self.view.wantsLayer = YES;
    self.view.layer.backgroundColor = NSColor.whiteColor.CGColor;
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@(10));
        make.bottom.right.equalTo(@(-10));
    }];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)openConfig:(id)sender{
    
}

#pragma mark - delegate
- (NSInteger)collectionView:(NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.tools.count;
}
- (NSCollectionViewItem *)collectionView:(NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(NSIndexPath *)indexPath{
    HomeToolItemView *itemView = [collectionView makeItemWithIdentifier:@"HomeToolItemView" forIndexPath:indexPath];
    
    [itemView configModel:[self.tools objectAtIndex:indexPath.item]];
    
    return itemView;
}

- (void)collectionView:(NSCollectionView *)collectionView didSelectItemsAtIndexPaths:(NSSet<NSIndexPath *> *)indexPaths{
    for (NSIndexPath *indexPath in indexPaths) {
        [collectionView deselectItemsAtIndexPaths:[NSSet setWithObject:indexPath]];
        HomeToolItemModel *toolModel = [self.tools objectAtIndex:indexPath.item];
        [toolModel showWindow];
    }
}
#pragma mark - Getter

- (NSCollectionView *)collectionView {
    if (!_collectionView) {
        NSCollectionViewFlowLayout *layout = [[NSCollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(60, 80);
        layout.sectionInset = NSEdgeInsetsMake(10, 10, 10, 10);
        layout.minimumInteritemSpacing = 10.0;
        layout.minimumLineSpacing = 10.0;
        _collectionView = [[NSCollectionView alloc] init];
        _collectionView.wantsLayer = YES;
        _collectionView.layer.backgroundColor = NSColor.redColor.CGColor;
        _collectionView.layer.cornerRadius = 5;
        _collectionView.layer.shadowOffset = CGSizeMake(0, 9);
        _collectionView.layer.shadowColor = NSColor.blackColor.CGColor;
        _collectionView.layer.shadowOpacity = 0.1;
        _collectionView.layer.shadowRadius = 5;
        _collectionView.collectionViewLayout = layout;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.selectable = YES;
        
        [_collectionView registerClass:HomeToolItemView.class forItemWithIdentifier:@"HomeToolItemView"];
    }
    return _collectionView;
}
@end
