//
//  AppDelegate.h
//  UIHelper
//
//  Created by Mac on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic, strong) NSWindow *mainWindow;

@end

