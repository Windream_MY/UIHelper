//
//  PresentCustomAnimator.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/15.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AppKit;

NS_ASSUME_NONNULL_BEGIN

@interface PresentCustomAnimator : NSObject<NSViewControllerPresentationAnimator>

@end

NS_ASSUME_NONNULL_END
