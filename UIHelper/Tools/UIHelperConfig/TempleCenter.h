//
//  TempleCenter.h
//  UIHelperExtension
//
//  Created by Mac on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSProperty.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^Comletion)(NSError *_Nullable error);

@interface TempleObeject : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *templateCodeStr;
@property (nonatomic, assign) BOOL isUIClass;

+ (TempleObeject *)creatTemplateObject;

@end

@interface TempleCenter : NSObject

@property (nonatomic, copy) NSArray<TempleObeject *> *templates;

+ (instancetype)shareInstance;

- (void)initData;
/// 添加或修改模板
/// @param object 模板对象
/// @param comletion 添加结果
- (void)modifyTemplatesObject:(TempleObeject *)object
                 comletion:(Comletion)comletion;

/// 移除模板
/// @param object 模板对象
/// @param comletion 添加结果
- (void)removeTemplatesObject:(TempleObeject *)object
                    comletion:(Comletion)comletion;

/// 移除模板
/// @param index 位置
/// @param comletion 添加结果
- (void)removeTemplatesIndex:(NSInteger )index
                    comletion:(Comletion)comletion;

/// 配置属性
/// 主要是读取配置信息写入属性中，用于后续生成代码判断
/// @param prop 属性
- (void)configProp:(PSProperty *)prop;

#pragma mark - 导入导出
- (void)putInConfigWithPath:(NSString *)path
                  comletion:(Comletion)comletion;
- (void)putOutConfigWithPath:(NSString *)path
                   comletion:(Comletion)comletion;
@end

NS_ASSUME_NONNULL_END
