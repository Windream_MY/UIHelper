//
//  UIHelperConfigWindowController.m
//  UIHelper
//
//  Created by 孟雷 on 2020/6/1.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "UIHelperConfigWindowController.h"
#import "AppDelegate.h"
#import "TempleCenter.h"
#import "UIHelperConfigEditView.h"

@interface UIHelperConfigWindowController ()

@property (weak) IBOutlet NSTableView *tabview;
@property (weak) IBOutlet NSTextField *currentClassNameLB;
@property (weak) IBOutlet NSButton *currentClassIsUICheckBT;
@property (weak) IBOutlet UIHelperConfigEditView *codeView;
@property (nonatomic, strong) TempleObeject *currentModel;

@end

@implementation UIHelperConfigWindowController

static UIHelperConfigWindowController *_instance = nil;

+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[UIHelperConfigWindowController alloc] initWithWindowNibName:NSStringFromClass(self.class)];
    });

    return _instance;
}

- (void)windowDidLoad {
    [super windowDidLoad];
    [self.tabview reloadData];
}

- (void)close {
    [super close];
    [self cancelEdit:nil];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate.mainWindow makeKeyAndOrderFront:nil];
}

#pragma mark - action

- (IBAction)addTemplate:(id)sender {
    [self cancelEdit:nil];

    TempleObeject *object = [TempleObeject creatTemplateObject];
    self.currentModel = object;
    self.currentClassNameLB.stringValue = object.name;
    self.currentClassIsUICheckBT.state = object.isUIClass;
    [self.codeView setContnet:object.templateCodeStr];
}

- (IBAction)deleteTemplate:(id)sender {
    NSInteger row = [self.tabview selectedRow];
    if (row >= 0) {
        @weakify(self);
        [[TempleCenter shareInstance] removeTemplatesIndex:row comletion:^(NSError *_Nullable error) {
            @strongify(self);
            if (error) {
                [self showErrorWithTip:@"删除失败"];
            } else {
                [self cancelEdit:nil];
                [self.tabview reloadData];
            }
        }];
    }
}

- (IBAction)saveTemplate:(id)sender {
    if (self.currentModel) {
        self.currentModel.name = self.currentClassNameLB.stringValue;
        self.currentModel.isUIClass = self.currentClassIsUICheckBT.state;
        self.currentModel.templateCodeStr = self.codeView.content;
        @weakify(self);
        [[TempleCenter shareInstance] modifyTemplatesObject:self.currentModel comletion:^(NSError *_Nullable error) {
            @strongify(self);
            if (error) {
                [self showErrorWithTip:@"保存失败"];
            } else {
                [self.tabview reloadData];
            }
        }];
    }
}

- (IBAction)cancelEdit:(id)sender {
    [self.tabview deselectAll:nil];
    self.currentModel = nil;
    [self.codeView setContnet:@""];
    self.currentClassNameLB.stringValue = @"";
    self.currentClassIsUICheckBT.state = YES;
}

- (IBAction)putinConfig:(id)sender {
    NSOpenPanel *panel = [NSOpenPanel openPanel];

    [panel setCanChooseFiles:YES];//是否能选择文件file

    [panel setCanChooseDirectories:NO];//是否能打开文件夹

    [panel setAllowsMultipleSelection:NO];//是否允许多选file

    NSInteger finded = [panel runModal]; //获取panel的响应

    if (finded == NSModalResponseOK) {
        if (panel.URL) {
            @weakify(self);
            [[TempleCenter shareInstance] putInConfigWithPath:panel.URL.relativePath comletion:^(NSError *_Nullable error) {
                @strongify(self);
                if (error) {
                    [self showErrorWithTip:@"导入失败"];
                } else {
                    [self showErrorWithTip:@"导入成功"];
                    [self.tabview reloadData];
                }
            }];
        }
    }
}

- (IBAction)putOutConfig:(id)sender {
    NSSavePanel *panel = [NSSavePanel savePanel];
    [panel setMessage:@"导出配置文件"];
    [panel setPrompt:NULL];
    [panel setAllowedFileTypes:@[@"json"]];
    [panel setCanCreateDirectories:YES];
    [panel setCanSelectHiddenExtension:YES];
    [panel setTitle:@"导出配置文件"];
    [panel setNameFieldStringValue:@"template"];
    [panel setDirectoryURL:NULL];
    NSInteger result = [panel runModal];
    if (result == NSModalResponseOK) {
        NSString *filePath = [[panel URL] relativePath];
        @weakify(self);
        [[TempleCenter shareInstance] putOutConfigWithPath:filePath comletion:^(NSError *_Nullable error) {
            @strongify(self);
            if (error) {
                [self showErrorWithTip:@"导出失败"];
            } else {
                [self showErrorWithTip:@"导出成功"];
            }
        }];
    } else {
        NSLog(@"Choose Cancle! ");
    }
}

- (void)showErrorWithTip:(NSString *)tip {
    NSAlert *alert = [[NSAlert alloc]init];
    alert.messageText = @"提示";
    alert.alertStyle = NSAlertStyleInformational;
    [alert addButtonWithTitle:@"确认"];
    [alert setInformativeText:tip];
    [alert beginSheetModalForWindow:self.window completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseOK) {
            NSLog(@"(returnCode == NSOKButton)");
        } else if (returnCode == NSModalResponseCancel) {
            NSLog(@"(returnCode == NSCancelButton)");
        } else if (returnCode == NSAlertFirstButtonReturn) {
            NSLog(@"if (returnCode == NSAlertFirstButtonReturn)");
        } else if (returnCode == NSAlertSecondButtonReturn) {
            NSLog(@"else if (returnCode == NSAlertSecondButtonReturn)");
        } else if (returnCode == NSAlertThirdButtonReturn) {
            NSLog(@"else if (returnCode == NSAlertThirdButtonReturn)");
        } else {
            NSLog(@"All Other return code %ld", (long)returnCode);
        }
    }];
}

- (void)controlTextDidEndEditing:(NSNotification *)obj {
    id text = obj.object;
    if (text == self.currentClassNameLB) {
        NSString *codeStr = self.codeView.content;
        if (codeStr.length > 0) {
            NSString *result = [codeStr stringByReplacingOccurrencesOfString:self.currentModel.name withString:self.currentClassNameLB.stringValue];
            self.currentModel.name = self.currentClassNameLB.stringValue;
            [self.codeView setContnet:result];
        }
    }
}

#pragma mark - TableViewDelegate and TableViewDataSource
- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    //获取row数据
    TempleObeject *object = [TempleCenter shareInstance].templates[row];
    //表格列的标识
    NSString *identifier = tableColumn.identifier;
    //根据表格列的标识,创建单元视图
    NSView *view = [tableView makeViewWithIdentifier:identifier owner:self];
    NSArray *subviews = [view subviews];
    if ([subviews count] > 0) {
        if ([identifier isEqualToString:@"classNameIdentifier"]) {
            //获取文本控件
            NSTextField *textField = subviews[0];
            textField.stringValue = object.name;
        }

        if ([identifier isEqualToString:@"isUIClassIdentifier"]) {
            //获取文本控件
            NSButton *checkBoxField = (NSButton *)view;
            checkBoxField.state = object.isUIClass;
            if (object.isUIClass) {
                checkBoxField.image = [NSImage imageNamed:NSImageNameStatusAvailable];
            } else {
                checkBoxField.image = [NSImage imageNamed:NSImageNameStatusUnavailable];
            }
        }
    }
    return view;
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    //返回表格共有多少行数据
    return [[TempleCenter shareInstance].templates count];
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    TempleObeject *object = [[TempleCenter shareInstance].templates objectAtIndex:row];
    self.currentModel = object;
    self.currentClassNameLB.stringValue = object.name;
    self.currentClassIsUICheckBT.state = object.isUIClass;
    [self.codeView setContnet:object.templateCodeStr];
    return YES;
}

@end
