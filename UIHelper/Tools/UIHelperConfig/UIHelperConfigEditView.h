//
//  UIHelperConfigEditView.h
//  UIHelper
//
//  Created by 孟雷 on 2020/6/1.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIHelperConfigEditView : NSView

@property (nonatomic, copy) NSString *content;

- (void)setContnet:(NSString *)content;

@end

NS_ASSUME_NONNULL_END
