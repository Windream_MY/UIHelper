//
//  UIHelperConfigEditView.m
//  UIHelper
//
//  Created by 孟雷 on 2020/6/1.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "UIHelperConfigEditView.h"
#import "ACEView.h"

@interface UIHelperConfigEditView ()

@property (nonatomic, strong) ACEView *codeView;

@end

@implementation UIHelperConfigEditView

- (void)viewDidMoveToWindow {
    self.wantsLayer = YES;
    self.layer.backgroundColor = [NSColor colorWithCalibratedRed:0.220 green:0.224 blue:0.231 alpha:0.02].CGColor;
    [self configUI];
}

- (void)configUI {
    [self addSubview:self.codeView];
    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.bottom.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
}

- (void)setContnet:(NSString *)content {
    self.codeView.string = content;
}

- (NSString *)content{
    return self.codeView.string;
}

- (ACEView *)codeView {
    if (!_codeView) {
        _codeView = [[ACEView alloc] initWithFrame:CGRectZero];
        [_codeView setDelegate:self];
        [_codeView setMode:ACEModeObjC];
        [_codeView setTheme:ACEThemeTwilight];
        [_codeView setKeyboardHandler:ACEKeyboardHandlerAce];
        [_codeView setShowPrintMargin:NO];
        [_codeView setShowInvisibles:YES];
        [_codeView setBasicAutoCompletion:YES];
        [_codeView setLiveAutocompletion:YES];
        [_codeView setSnippets:YES];
        [_codeView setEmmet:YES];
        [_codeView focus];
    }
    return _codeView;
}
@end
