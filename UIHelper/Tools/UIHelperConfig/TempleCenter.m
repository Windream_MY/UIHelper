//
//  TempleCenter.m
//  UIHelperExtension
//
//  Created by Mac on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import "TempleCenter.h"
#import "NSString+Add.h"
#import "YYModel.h"
static NSString * const kTempProp = @"#PROPERTY#";
static NSString * const kTempSection = @"#Section#";
static NSString * const kTempSectionEnd = @"#SectionEnd#";
//static NSString * const kTempProp = @"#PROPERTY#";

static TempleCenter *_instance;
NSString *tempCodeStr = @"";

@implementation TempleObeject

+ (TempleObeject *)creatTemplateObject{
    TempleObeject *object = [TempleObeject new];
    object.name = @"ClassName";
    object.isUIClass = YES;
    if (tempCodeStr.length == 0) {
        NSString *file = [[NSBundle mainBundle] pathForResource:@"template" ofType:@"txt"];
        tempCodeStr = [NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil];
    }
    object.templateCodeStr = tempCodeStr;
    
    return object;
}

@end

@interface TempleCenter ()

@end

@implementation TempleCenter

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[TempleCenter alloc] init];
        [_instance initData];
    });
    return _instance;
}

- (void)initData{
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:[self filePath]];
    
    if (data.length == 0) {
        // copy 初始化数据
        data = [NSData dataWithContentsOfURL:[NSBundle.mainBundle URLForResource:@"template" withExtension:@"json"]];
    }
    
    NSArray<TempleObeject *> *list = [NSArray yy_modelArrayWithClass:TempleObeject.class json:data];
    self.templates = [list copy];
}

- (void)modifyTemplatesObject:(TempleObeject *)object
                 comletion:(Comletion)comletion{
    if (object && [object isKindOfClass:TempleObeject.class]) {
        NSMutableArray *tempList = [NSMutableArray arrayWithArray:self.templates];
        TempleObeject *exsitTemp = [self isExsit:object];
        if (exsitTemp) {
            exsitTemp.isUIClass = object.isUIClass;
            exsitTemp.templateCodeStr = object.templateCodeStr;
        }else{
            [tempList addObject:object];
        }
        
        NSData *json_data = [tempList yy_modelToJSONData];
        BOOL success =   [json_data writeToFile:[self filePath] atomically:YES];
        if (success) {
            if (comletion) {
                self.templates = tempList.copy;
                comletion(nil);
            }
        }else{
            if (comletion) {
                comletion([NSError new]);
            }
        }
    }else{
        if (comletion) {
            comletion([NSError new]);
        }
    }
}

- (void)removeTemplatesIndex:(NSInteger )index
                   comletion:(Comletion)comletion{
    if (index >= 0) {
        TempleObeject *object = [self.templates objectAtIndex:index];
        [self removeTemplatesObject:object comletion:comletion];
    }
}

- (void)removeTemplatesObject:(TempleObeject *)object
                    comletion:(Comletion)comletion{
    if (object && [object isKindOfClass:TempleObeject.class]) {
        NSMutableArray *tempList = [NSMutableArray arrayWithArray:self.templates];
        TempleObeject *exsitTemp = [self isExsit:object];
        if (exsitTemp) {
            [tempList removeObject:exsitTemp];
            NSData *json_data = [tempList yy_modelToJSONData];
            BOOL success =   [json_data writeToFile:[self filePath] atomically:YES];
            if (success) {
                if (comletion) {
                    self.templates = tempList.copy;
                    comletion(nil);
                }
            }else{
                if (comletion) {
                    comletion([NSError new]);
                }
            }
        }else{
            if (comletion) {
                comletion([NSError new]);
            }
        }
    }else{
        if (comletion) {
            comletion([NSError new]);
        }
    }
}

- (void)putInConfigWithPath:(NSString *)path
                  comletion:(Comletion)comletion{
    if (path.length > 0) {
        NSData *data = [[NSData alloc] initWithContentsOfFile:path];
        if (data.length>0) {

            BOOL success = [data writeToFile:[self filePath] atomically:YES];
            if (success) {
                [self initData];
                if (comletion) {
                    comletion(nil);
                }
            }else{
                if (comletion) {
                    comletion([NSError new]);
                }
            }
        }else{
            if (comletion) {
                comletion([NSError new]);
            }
        }
    }else{
        if (comletion) {
            comletion([NSError new]);
        }
    }
}

- (void)putOutConfigWithPath:(NSString *)path
                   comletion:(Comletion)comletion{
    if (path.length > 0) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
            [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
        }
        NSData *data = [[NSData alloc] initWithContentsOfFile:[self filePath]];
        BOOL success = [data writeToFile:path atomically:YES];
        if (!success) {
            if (comletion) {
                comletion([NSError new]);
            }
        }else{
            if (comletion) {
                comletion(nil);
            }
        }
    }else{
        if (comletion) {
            comletion([NSError new]);
        }
    }
}

- (TempleObeject *)isExsit:(TempleObeject *)object{
    __block TempleObeject *temp = nil;
    [self.templates enumerateObjectsUsingBlock:^(TempleObeject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.name isEqualToString:object.name]) {
            temp = obj;
            *stop = YES;
        }
    }];
    
    return temp;
}

- (NSString *)filePath{
    NSURL *groupURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:@"8QA3QVY82W.UIHelper"];
    NSURL *fileURL = [groupURL URLByAppendingPathComponent:@"template.json"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fileURL path]]) {
        [[NSFileManager defaultManager] createFileAtPath:[fileURL path] contents:nil attributes:nil];
    }
    NSLog(@"%@",fileURL.path);
    return fileURL.path;
}

- (void)configProp:(PSProperty *)prop{
    if (!prop) {
        return;
    }
    
    for (TempleObeject *temp in self.templates) {
        if ([temp.name isEqualToString:prop.dataType]) {
            prop.isUIClass = temp.isUIClass;
            prop.getterStr = [temp.templateCodeStr stringByReplacingOccurrencesOfString:kTempProp withString:prop.name];
            break;
        }
    }
}
@end
