//
//  MainWindowController.h
//  UIHelper
//
//  Created by 孟雷 on 2020/6/1.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainWindowController : NSWindowController

@end

NS_ASSUME_NONNULL_END
