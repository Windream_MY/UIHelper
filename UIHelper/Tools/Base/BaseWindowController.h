//
//  BaseWindowController.h
//  UIHelper
//
//  Created by 孟雷 on 2020/6/1.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseWindowController : NSWindowController

+ (instancetype)shareInstance;

@end

NS_ASSUME_NONNULL_END
