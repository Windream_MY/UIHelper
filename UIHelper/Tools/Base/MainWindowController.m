//
//  MainWindowController.m
//  UIHelper
//
//  Created by 孟雷 on 2020/6/1.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "MainWindowController.h"

@interface MainWindowController ()

@end

@implementation MainWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
