//
//  XIBTransferViewController.m
//  UIHelper
//
//  Created by 孟雷 on 2020/5/15.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "XIBTransferViewController.h"
#import "XIBFileItemCell.h"
#import "XIBFileItemModel.h"
#import "XIBManager.h"

@interface XIBTransferViewController ()

@property (weak) IBOutlet NSCollectionView *collectionView;


@end

@implementation XIBTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self configUI];
    [self configData];
}

- (void)configUI{
    
}

- (void)configData{
    [self.collectionView registerClass:XIBFileItemCell.class forItemWithIdentifier:@"XIBFileItemCell"];
}

- (IBAction)addFile:(id)sender {
    NSOpenPanel *panel = [NSOpenPanel openPanel];

    [panel setCanChooseFiles:YES];//是否能选择文件file

    [panel setCanChooseDirectories:YES];//是否能打开文件夹

    [panel setAllowsMultipleSelection:YES];//是否允许多选file

    NSInteger finded = [panel runModal]; //获取panel的响应

    if (finded == NSModalResponseOK) {
        [[XIBManager sharedInstance] addXibFileWithURLS:panel.URLs];
        [self.collectionView reloadData];
    }
}

- (IBAction)startTransfer:(id)sender {
    
}

#pragma mark - delegate
- (NSInteger)collectionView:(NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [XIBManager sharedInstance].XIBFiles.count;
}
- (NSCollectionViewItem *)collectionView:(NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(NSIndexPath *)indexPath{
    XIBFileItemCell *itemView = [collectionView makeItemWithIdentifier:@"XIBFileItemCell" forIndexPath:indexPath];
    if (!itemView) {
        itemView = [XIBFileItemCell new];
    }
    [itemView configModel:[[XIBManager sharedInstance].XIBFiles objectAtIndex:indexPath.item]];
    
    return itemView;
}

- (void)collectionView:(NSCollectionView *)collectionView didSelectItemsAtIndexPaths:(NSSet<NSIndexPath *> *)indexPaths{
    for (NSIndexPath *indexPath in indexPaths) {
        NSLog(@"111%ld",(long)indexPath.item);
    }
}
@end
