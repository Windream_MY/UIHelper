//
//  XIBManager.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/21.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XIBFileItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XIBManager : NSObject

@property (nonatomic, copy) NSArray<XIBFileItemModel *> *XIBFiles;

+ (instancetype)sharedInstance;

/// 添加 XIB 文件
/// @param files 要添加的文件
- (void)addXibFiles:(NSArray<XIBFileItemModel *> *)files;

/// 添加 XIB 文件
/// @param urls 要添加的文件的 url 列表
- (void)addXibFileWithURLS:(NSArray<NSURL *> *)urls;

/// 删除 XIB 文件
/// @param files 要删除的文件
- (void)removeXibFiles:(NSArray<XIBFileItemModel *> *)files;

- (void)startTransfer;

- (void)stopTransfer;
@end

NS_ASSUME_NONNULL_END
