//
//  XIBManager.m
//  UIHelper
//
//  Created by 孟雷 on 2020/5/21.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "XIBManager.h"

@interface XIBManager()

@property (nonatomic, strong) NSMutableArray<XIBFileItemModel *> *p_XIBFiles;

@end

@implementation XIBManager

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static XIBManager *instance = nil;
    dispatch_once(&onceToken,^{
        instance = [XIBManager new];
        [instance initConfig];
    });
    return instance;
}

- (void)initConfig{
    self.p_XIBFiles = [NSMutableArray<XIBFileItemModel *> new];
}

- (void)addXibFiles:(NSArray<XIBFileItemModel *> *)files{
    
    for (XIBFileItemModel *model in files) {
        if (![self hasExsit:model]) {
            [self.p_XIBFiles addObject:model];
        }
    }
}

- (void)addXibFileWithURLS:(NSArray<NSURL *> *)urls{
    for (NSURL *url in urls) {
        if ([url.absoluteString hasSuffix:@".xib"]) {
            XIBFileItemModel *model = [XIBFileItemModel new];
            model.xibFilePath = url;
            [self addXibFiles:@[model]];
        }
    }
}

- (void)removeXibFiles:(NSArray<XIBFileItemModel *> *)files{
    
    for (XIBFileItemModel *model in files) {
        if ([self hasExsit:model]) {
            [self.p_XIBFiles removeObject:model];
        }
    }
}

- (void)startTransfer{
    
}

- (void)stopTransfer{
    
}

- (BOOL)hasExsit:(XIBFileItemModel *)model{
    BOOL hasExsit = NO;
    
    for (XIBFileItemModel* item in self.p_XIBFiles) {
        if ([item.xibFileName isEqualToString:model.xibFileName]) {
            hasExsit = YES;
            break;
        }
    }
    
    return hasExsit;
}
#pragma mark - Getter
- (NSArray<XIBFileItemModel *> *)XIBFiles{
    return self.p_XIBFiles.copy;
}
@end
