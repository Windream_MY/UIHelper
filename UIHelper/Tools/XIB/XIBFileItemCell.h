//
//  XIBFileItemCell.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/15.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "XIBFileItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface XIBFileItemCell : NSCollectionViewItem

- (void)configModel:(XIBFileItemModel *)model;

@end

NS_ASSUME_NONNULL_END
