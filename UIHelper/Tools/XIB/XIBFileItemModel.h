//
//  XIBFileItemModel.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/15.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, XIBFileStatus) {
    XIBFileStatusDefult,
    XIBFileStatusTransfering,
    XIBFileStatusTransfered,
    XIBFileStatusError,
    XIBFileStatusOther
};

@interface XIBFileItemModel : NSObject

@property (nonatomic, copy) NSURL *xibFilePath;
@property (nonatomic, copy) NSString *xibFileName;
@property (nonatomic, assign) XIBFileStatus status;
@property (nonatomic, copy) NSURL *codeFilePath;

@end

NS_ASSUME_NONNULL_END
