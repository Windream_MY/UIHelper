//
//  XIBFileItemModel.m
//  UIHelper
//
//  Created by 孟雷 on 2020/5/15.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "XIBFileItemModel.h"

@implementation XIBFileItemModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.status = XIBFileStatusDefult;
    }
    return self;
}

- (NSString *)xibFileName{
    NSString *name = @"";
    if (self.xibFilePath.absoluteString.length > 0) {
        NSArray *path = [self.xibFilePath.absoluteString componentsSeparatedByString:@"/"];
        if (path.lastObject) {
            name = [path.lastObject componentsSeparatedByString:@"."].firstObject;
        }
    }
    return name;
}

@end
