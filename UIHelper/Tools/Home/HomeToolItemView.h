//
//  HomeToolItemView.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/13.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "HomeToolItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeToolItemView : NSCollectionViewItem

- (void)configModel:(HomeToolItemModel *)model;

@end

NS_ASSUME_NONNULL_END
