//
//  HomeToolItemModel.m
//  UIHelper
//
//  Created by 孟雷 on 2020/5/14.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "HomeToolItemModel.h"

@implementation HomeToolItemModel
+ (HomeToolItemModel *)itemWithTitle:(NSString *)title iconName:(NSString *)iconName controller:(NSWindowController *) controller{
    HomeToolItemModel *model = [HomeToolItemModel new];
    model.title = title;
    model.iconName = iconName;
    model.controller = controller;
    
    return model;
}

- (void)showWindow{
    self.controller.window.title = self.title;
    [self.controller.window center];
    [self.controller.window makeKeyAndOrderFront:nil];
}
@end
