//
//  HomeToolItemView.m
//  UIHelper
//
//  Created by 孟雷 on 2020/5/13.
//  Copyright © 2020 Windream. All rights reserved.
//

#import "HomeToolItemView.h"
#import "CollectionViewItemView.h"

@interface HomeToolItemView ()<NSCollectionViewItemViewDelegate>

@property (nonatomic, strong) CollectionViewItemView *containerView;
@property(nonatomic, strong) NSImageView *icon;
@property(nonatomic, strong) NSTextField *titleLB;
@property(nonatomic, strong) HomeToolItemModel *viewModel;

@end

@implementation HomeToolItemView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [self configUI];
}

- (void)configUI {
    
    [self.view addSubview:self.containerView];
    [self.containerView addSubview:self.icon];
    [self.containerView addSubview:self.titleLB];
    
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.equalTo(@(0));
    }];
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(@(CGSizeMake(40, 40)));
        make.centerX.equalTo(@(0));
        make.top.equalTo(@(10));
    }];
    
    [self.titleLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(5));
        make.right.equalTo(@(-5));
        make.bottom.equalTo(@(-10));
        make.height.equalTo(@(18));
    }];
}

- (void)configModel:(HomeToolItemModel *)model{
    self.viewModel = model;
    
    self.imageView.image = [NSImage imageNamed:model.iconName];
    self.titleLB.stringValue = model.title;
}

-(void)showDetailWindow{
//    if (self.viewModel.controller) {
//        [self.viewModel.controller.window makeKeyAndOrderFront:self];
//    }
}

-(void)setSelected:(BOOL)selected
{
    [self.containerView setIsSelected:selected];
    [super setSelected:NO];
}


#pragma mark - Getter


- (CollectionViewItemView *)containerView {
    if (!_containerView) {
        _containerView = [CollectionViewItemView new];
        _containerView.wantsLayer = YES;
        _containerView.layer.backgroundColor = [NSColor systemBlueColor].CGColor;
        _containerView.delegate = self;
    }
    return _containerView;
}

- (NSImageView *)icon {
    if (!_icon) {
        _icon = [[NSImageView alloc] init];
    }
    return _icon;
}

- (NSTextField *)titleLB {
    if (!_titleLB) {
        _titleLB = [[NSTextField alloc] init];
        _titleLB.backgroundColor = NSColor.clearColor;
        _titleLB.alignment = NSTextAlignmentCenter;
        _titleLB.font = PFSC_S(10);
        _titleLB.textColor = NSColorFromRGB(0x101D37);
        _titleLB.editable = NO;
        _titleLB.selectable = NO;
        _titleLB.bezeled = NO;
    }
    return _titleLB;
}
@end
