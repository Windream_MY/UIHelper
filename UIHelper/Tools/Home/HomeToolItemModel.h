//
//  HomeToolItemModel.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/14.
//  Copyright © 2020 Windream. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeToolItemModel : NSObject

@property(nonatomic, copy) NSString *iconName;
@property(nonatomic, copy) NSString *title;
@property (nonatomic, copy) dispatch_block_t clickAction;
@property (nonatomic, strong) NSWindowController *controller;

+ (HomeToolItemModel *)itemWithTitle:(NSString *)title
                            iconName:(NSString *)iconName
                          controller:(NSWindowController *)controller;

- (void)showWindow;
@end

NS_ASSUME_NONNULL_END
