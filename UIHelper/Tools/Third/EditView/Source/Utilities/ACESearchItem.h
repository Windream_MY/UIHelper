//
//  ACEObject.h
//  ACEView
//
//  Created by Jan Gassen on 10/01/15.
//  Copyright (c) 2015 Code of Interest. All rights reserved.
//
#import <Cocoa/Cocoa.h>
#ifndef ACEView_ACEObject_h
#define ACEView_ACEObject_h

@interface ACESearchItem : NSObject
@property (nonatomic, assign)NSInteger startColumn;
@property (nonatomic, assign)NSInteger startRow;
@property (nonatomic, assign)NSInteger endColumn;
@property (nonatomic, assign)NSInteger endRow;

+ (NSArray*) fromString:(NSString*)text;

@end


#endif
