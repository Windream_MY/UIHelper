//
//  Marco.h
//  UIHelper
//
//  Created by 孟雷 on 2020/5/13.
//  Copyright © 2020 Windream. All rights reserved.
//

#ifndef Marco_h
#define Marco_h

#ifndef weakify
#if DEBUG
#if __has_feature(objc_arc)
#define weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
#else
#define weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
#endif
#else
#if __has_feature(objc_arc)
#define weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
#else
#define weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
#endif
#endif
#endif

#ifndef strongify
#if DEBUG
#if __has_feature(objc_arc)
#define strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
#else
#define strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
#endif
#else
#if __has_feature(objc_arc)
#define strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
#else
#define strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
#endif
#endif
#endif

#define PFSC_L(fontSize) [NSFont fontWithName:@"PingFangSC-Light" size:(fontSize)]
#define PFSC_R(fontSize) [NSFont fontWithName:@"PingFangSC-Regular" size:(fontSize)]
#define PFSC_M(fontSize) [NSFont fontWithName:@"PingFangSC-Medium" size:(fontSize)]
#define PFSC_S(fontSize) [NSFont fontWithName:@"PingFangSC-Semibold" size:(fontSize)]

#define NSColorFromRGB(rgbValue)                                         \
[NSColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0    \
blue:((float)(rgbValue & 0xFF)) / 255.0             \
alpha:1.0]

#define NSColorFromRGBA(rgbValue, alphaValue)                            \
[NSColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8)) / 255.0  \
blue:((float)(rgbValue & 0x0000FF)) / 255.0         \
alpha:alphaValue]

#endif /* Marco_h */
