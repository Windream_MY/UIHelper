//
//  NSString+Add.h
//  UIHelperExtension
//
//  Created by 孟雷 on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import <AppKit/AppKit.h>


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Add)

- (NSArray<NSString *> *)matchesWithRegularString:(NSString *)regularString;

@end

NS_ASSUME_NONNULL_END
