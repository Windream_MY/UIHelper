//
//  NSString+Add.m
//  UIHelperExtension
//
//  Created by 孟雷 on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import "NSString+Add.h"

#import <AppKit/AppKit.h>


@implementation NSString (Add)
- (NSArray<NSString *> *)matchesWithRegularString:(NSString *)regularString{
    NSMutableArray *matchesArray = [NSMutableArray new];
    
    if (self.length > 0 && regularString.length>0) {
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regularString options:0 error:nil];
        NSArray *matches = [regex matchesInString:self options:0 range:NSMakeRange(0, self.length)];

        for (int i = 0; i < matches.count; i++) {
            NSRange range = [matches[i] range];
            if (range.length > 0) {
                NSString *string = [self substringWithRange:range];
                [matchesArray addObject:string];
            }
        }
    }
    
    return matchesArray;
}
@end
