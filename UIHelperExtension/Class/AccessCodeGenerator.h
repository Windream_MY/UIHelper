//
//  AccessCodeGenerator.h
//  PSCodeGenerator
//
//  Created by Pan on 2017/5/15.
//  Copyright © 2017年 Sheng Pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSProperty.h"

typedef NS_ENUM(NSUInteger, SuperViewType) {
    superViewTypeView,
    superViewTypeVCell,
    superViewTypeVController,
};

@interface AccessCodeGenerator : NSObject

/**
 pattern matching the string, recognize Objective-C propertyies, generate their lazy getter code and return.

 basic data type and id will be ignored.
 
 @param string a string
 @return lazy getter code
 */
+ (NSArray<NSString *> *)lazyGetterForString:(NSString *)string;
+ (NSArray<NSString *> *)lazyGetterForString:(NSString *)string superViewType:(SuperViewType)superViewType;

/**
 pattern matching the string, recognize Objective-C propertyies, generate their setter code and return.
 
 @param string a string
 @return setter code
 */
+ (NSArray<NSString *> *)setterForString:(NSString *)string;

@end
