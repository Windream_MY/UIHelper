//
//  AccessCodeGenerator.m
//  PSCodeGenerator
//
//  Created by Pan on 2017/5/15.
//  Copyright © 2017年 Sheng Pan. All rights reserved.
//

#import "AccessCodeGenerator.h"
#import "TempleCenter.h"

static SuperViewType _superViewType = superViewTypeView;

@implementation AccessCodeGenerator

+ (NSArray<NSString *> *)lazyGetterForString:(NSString *)string {
    return [self lazyGetterForString:string superViewType:superViewTypeView];
}

+ (NSArray<NSString *> *)lazyGetterForString:(NSString *)string superViewType:(SuperViewType)superViewType{
    _superViewType = superViewType;
    NSArray<PSProperty *> *props = [self propertyWithContent:string];
    NSMutableArray *result = [NSMutableArray arrayWithArray:[self autoCodeWithPSPropertys:props]];
    NSArray *reverseArray = [[result reverseObjectEnumerator] allObjects];
    return reverseArray;
}

+ (NSArray<NSString *> *)setterForString:(NSString *)string {
    NSArray<PSProperty *> *props = [self propertyWithContent:string];
    NSMutableArray *result = [NSMutableArray array];
    for (PSProperty *model in props) {
        [result addObject:[self setterWithPSProperty:model]];
    }
    return result;
}

+ (NSArray<PSProperty *> *)propertyWithContent:(NSString *)content
{
    [[TempleCenter shareInstance] initData];
    NSMutableArray *result = [NSMutableArray array];
    NSString * regularStr = @"@property([\\s\\S]*?);";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regularStr options:0 error:nil];
    NSArray *matches = [regex matchesInString:content options:0 range:NSMakeRange(0, content.length)];
    
    for (int i = 0; i < matches.count; i++) {
        
        NSRange propertyRange = [matches[i] range];
        if (propertyRange.length > 0) {
            
            NSString *propertyString = [content substringWithRange:propertyRange];
            if ([propertyString containsString:IB_OUTLET]) {
                continue;
            }
            
            NSArray *keywords = [self propertysKeywordsWithPropertyString:propertyString.copy];
            NSArray *dataTypeAndName = [self propertyTypeAndNameWithProperty:propertyString.copy];
            
            if (!dataTypeAndName.count)
            {
                continue;
            }
            // “NSArray<NSArray<NSString" ">"">""name"
            PSProperty * proMoel = [[PSProperty alloc] init];
            proMoel.keywords = keywords;
            proMoel.dataType      = [[dataTypeAndName subarrayWithRange:NSMakeRange(0, dataTypeAndName.count - 1)] componentsJoinedByString:@" *"];
            proMoel.name          = [dataTypeAndName lastObject];
            proMoel.isObjectType   = [propertyString containsString:@"*"];
            [[TempleCenter shareInstance] configProp:proMoel];
            [result addObject:proMoel];
        }
    }
    return result;
}


+ (NSArray<NSString *> *)propertysKeywordsWithPropertyString:(NSString *)propertyStr
{
    NSString * propertyStr1 = propertyStr;
    NSString * regularStr = @"\\(.*\\)";
    NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:regularStr options:0 error:nil];
    NSArray *matches1 = [regex1 matchesInString:propertyStr1 options:0 range:NSMakeRange(0, propertyStr1.length)];
    
    for (int i = 0; i < matches1.count; i++)
    {
        NSRange firstHalfRange = [matches1[i] range];
        if (firstHalfRange.length > 0)
        {
            NSString *resultString = [[propertyStr1 substringWithRange:firstHalfRange] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            resultString = [resultString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            resultString = [resultString stringByReplacingOccurrencesOfString:@")" withString:@""];
            resultString = [resultString stringByReplacingOccurrencesOfString:@" " withString:@""];
            return  [resultString componentsSeparatedByString:@","];
        }
    }
    return nil;
}

+ (NSMutableArray *)propertyTypeAndNameWithProperty:(NSString *)propertyStr;
{

    NSString * regularStr = @"\\).*\\;";
    NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:regularStr options:0 error:nil];
    NSArray *matches1 = [regex1 matchesInString:propertyStr options:0 range:NSMakeRange(0, propertyStr.length)];
    
    NSMutableArray *result = @[].mutableCopy;
    for (int i = 0 ; i < matches1.count; i++)
    {
        NSRange matchedRange =   [matches1[i] range];
        if (matchedRange.length > 0)
        {
            
            NSString *resultString = [[propertyStr substringWithRange:matchedRange]
                                      stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            // ) NSArray  <NSArray <  NSString *> *> *name;
            resultString = [resultString stringByReplacingOccurrencesOfString:@")" withString:@""];
            resultString = [resultString stringByReplacingOccurrencesOfString:@";" withString:@""];
            // NSArray  <NSArray <  NSString *> *> *name
            if([propertyStr containsString:@"*"])
            {
                // NSArray<NSArray<NSString*>*>*name
                resultString = [resultString stringByReplacingOccurrencesOfString:@" " withString:@""];
                result =  [resultString componentsSeparatedByString:@"*"].mutableCopy;
            }
            else
            {
                // 不带 * ，要么是 id 要么是基础类型，要么是宏定义的类型。
                result =  [resultString componentsSeparatedByString:@" "].mutableCopy;
                [result enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isEqualToString:@""]) {
                        [result removeObject:obj];
                    }
                }];
            }
            return result;
        }
    }
    return result;
}

+ (NSArray<NSString *> *)autoCodeWithPSPropertys:(NSArray<PSProperty *> *)props{
    NSMutableArray *aotuCodeLines = [NSMutableArray new];
    NSMutableArray *configSubviewLines = [NSMutableArray new];
    NSMutableArray *layoutSubviewLines = [NSMutableArray new];
    NSMutableArray *getterSubviewLines = [NSMutableArray new];
    
    [props enumerateObjectsUsingBlock:^(PSProperty * _Nonnull property, NSUInteger idx, BOOL * _Nonnull stop) {
        if (property.isUIClass) {
            [configSubviewLines addObject:[self configSubviewWithPSProperty:property]];
            [layoutSubviewLines addObject:[self layoutSubviewWithPSProperty:property]];
        }
        [getterSubviewLines addObject:[self getterWithPSProperty:property]];
    }];
    // 生成 configSubview
    if (configSubviewLines.count > 0) {
        [configSubviewLines insertObject:@"\n- (void)configUI {" atIndex:0];
        [configSubviewLines addObject:@"    [self layout];"];
        [configSubviewLines addObject:@"}"];
    }
    // 生成 layout
    if (layoutSubviewLines.count > 0) {
        [layoutSubviewLines insertObject:@"\n- (void)layout {" atIndex:0];
        [layoutSubviewLines addObject:@"}"];
    }
    // 生成 getter
    if (getterSubviewLines.count > 0) {
        [getterSubviewLines insertObject:@"\n#pragma mark - Getter" atIndex:0];
    }
    
    [aotuCodeLines addObjectsFromArray:configSubviewLines];
    [aotuCodeLines addObjectsFromArray:layoutSubviewLines];
    [aotuCodeLines addObjectsFromArray:getterSubviewLines];
    
    return aotuCodeLines.copy;
}

// 生成 configSubview
+ (NSString *)configSubviewWithPSProperty:(PSProperty *)model
{
    NSString *codeFromProp = @"";
    if (![model.keywords containsObject:ASSIGN]
        && ![model.dataType isEqualToString:ID]) {
        NSString *superView = @"self";
        if (_superViewType == superViewTypeVCell) {
            superView = @"self.contentView";
        }else if (_superViewType == superViewTypeVController){
            superView = @"self.view";
        }

        codeFromProp = [NSString stringWithFormat:@"    [%@ addSubview:self.%@];",superView,model.name];
    }

    return codeFromProp;
}

// 生成 layout
+ (NSString *)layoutSubviewWithPSProperty:(PSProperty *)model
{
    NSString *codeFromProp = @"";
    if (![model.keywords containsObject:ASSIGN]
        && ![model.dataType isEqualToString:ID]) {
        
        codeFromProp = [NSString stringWithFormat:@"    [self.%@ mas_makeConstraints:^(MASConstraintMaker *make) {\n        make.top.equalTo(@(\<#top\#>));\n        make.left.equalTo(@(\<#left#>\));\n        make.bottom.equalTo(@(\<#bottom#>\));\n        make.right.equalTo(@(\<#right#>\));\n    }];",model.name];
    }

    return codeFromProp;
}

// 生成 getter
+ (NSString *)getterWithPSProperty:(PSProperty *)model
{
    NSString *codeFromProp = @"";
    if (![model.keywords containsObject:ASSIGN]
        && ![model.dataType isEqualToString:ID]) {
        if (model.getterStr.length > 0) {
            codeFromProp = model.getterStr;
        }else{
            NSString *functionType = [model.keywords containsObject:CLASS] ? @"+" : @"-";
            codeFromProp = [NSString stringWithFormat:@"\n%@ (%@ *)%@ {\n    if (!_%@) {\n        _%@ = [[%@ alloc] init];\n    }\n    return _%@;\n}",
                          functionType,
                          model.dataType,
                          model.name,
                          model.name,
                          model.name,
                          model.dataType,
                          model.name];
        }
    }
    
    return codeFromProp;
}

+ (NSString *)setterWithPSProperty:(PSProperty *)model {
    NSString *setter = @"";
    if (model.name.length < 1) {
        return setter;
    }
    
    NSString *firstLetter = [model.name substringToIndex:1];
    NSString *name = [model.name stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstLetter.uppercaseString];
    NSString *functionType = [model.keywords containsObject:CLASS] ? @"+" : @"-";
    if (model.isObjectType) {
        setter = [NSString stringWithFormat:@"\n%@ (void)set%@:(%@ *)%@ {\n    _%@ = %@;\n}",
                  functionType,
                  name,
                  model.dataType,
                  model.name,
                  model.name,
                  model.name];
    } else {
        setter = [NSString stringWithFormat:@"\n%@ (void)set%@:(%@)%@ {\n    _%@ = %@;\n}",
                  functionType,
                  name,
                  model.dataType,
                  model.name,
                  model.name,
                  model.name];
    }
    return setter;
}
@end
