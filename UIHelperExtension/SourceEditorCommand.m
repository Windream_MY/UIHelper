//
//  SourceEditorCommand.m
//  UIHelperExtension
//
//  Created by Mac on 2019/11/6.
//  Copyright © 2019 Windream. All rights reserved.
//

#import "SourceEditorCommand.h"
#import "AccessCodeGenerator.h"
#import "TempleCenter.h"
#import <Cocoa/Cocoa.h>
@implementation SourceEditorCommand

- (void)performCommandWithInvocation:(XCSourceEditorCommandInvocation *)invocation completionHandler:(void (^)(NSError * _Nullable nilOrError))completionHandler
{
    if ([invocation.commandIdentifier isEqualToString:@"wdm.UIHelper.UIHelperExtension.AutoGenerator"])
    {
        [self performCreateCodeWithInvocation:invocation];
    }else if ([invocation.commandIdentifier isEqualToString:@"wdm.UIHelper.UIHelperExtension.AutoGeneratorConfig"]){
        NSString *appPath = @"/Applications/UIHelper.app";
        [[NSWorkspace sharedWorkspace]openFile:appPath];
    }
    
    completionHandler(nil);
}

- (void)performCreateCodeWithInvocation:(XCSourceEditorCommandInvocation *)invocation{
    // get selected String
    NSMutableArray *selections = [NSMutableArray array];
    for ( XCSourceTextRange *range in invocation.buffer.selections )
    {
        NSInteger startLine = range.start.line;
        // make user select line easier
        NSInteger endLine = range.end.column > 0 ? range.end.line + 1 : range.end.line;
        
        for ( NSInteger i = startLine; i < endLine ; i++)
        {
            [selections addObject:invocation.buffer.lines[i]];
        }
    }
    NSString *selectedString = [selections componentsJoinedByString:@""];
    
    // find the insert location
    NSString *interface = [self interfaceContainerInBuffer:invocation.buffer
                                               initialLine:invocation.buffer.selections.firstObject.start.line];
    NSInteger insertIndex = [self endLineOfImplementationForInterface:interface buffer:invocation.buffer];
    insertIndex = (insertIndex == NSNotFound)
    ? invocation.buffer.selections.lastObject.end.line + 1
    : insertIndex;
    
    SuperViewType superViewType = superViewTypeView;
    if ([interface hasSuffix:@"cell"] ||
        [interface hasSuffix:@"Cell"]) {
        superViewType = superViewTypeVCell;
    }else if ([interface hasSuffix:@"VC"] ||
              [interface hasSuffix:@"ViewController"] ||
              [interface hasSuffix:@"Controller"] ||
              [interface hasSuffix:@"controller"]){
        superViewType = superViewTypeVController;
    }
    NSArray *getter = [AccessCodeGenerator lazyGetterForString:selectedString superViewType:superViewType];
    for (NSInteger i = 0; i < getter.count; i++)
    {
        [invocation.buffer.lines insertObject:getter[i] atIndex:insertIndex];
    }
}

- (NSString *)interfaceContainerInBuffer:(XCSourceTextBuffer *)buffer initialLine:(NSInteger)line
{
    NSString *interfaceString = nil;
    while (line >= 0 && ![interfaceString containsString:@"@interface"])
    {
        interfaceString = buffer.lines[line];
        line--;
    }
    if (!interfaceString) {
        return nil;
    }
    
    NSArray *components = [interfaceString componentsSeparatedByString:@" "];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF != ''"];
    interfaceString = [components filteredArrayUsingPredicate:predicate][1];
    components = [interfaceString componentsSeparatedByString:@"()"];
    return components[0];
}

- (NSInteger)endLineOfImplementationForInterface:(NSString *)interface buffer:(XCSourceTextBuffer *)buffer
{
    NSInteger result = NSNotFound;
    BOOL foundImplementation = NO;
    for (NSInteger i = 0; i < buffer.lines.count; i++)
    {
        NSString *lineContent = buffer.lines[i];
        if (foundImplementation
            && [lineContent containsString:@"@end"]) {
            result = i;
            break;
        }
        
        if ([lineContent containsString:@"@implementation"]
            && [lineContent containsString:interface]) {
            foundImplementation = YES;
        }
        
    }
    return result;
}
@end
